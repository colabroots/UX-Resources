**Co-Lab Roots**

**Resources for "What You Don't Know Can Hurt You: UX Essentials for The Web"**

Course Description:
This two-day workshop will offer an overview of user experience design through discussions and hands-on-exercises that can be applied to a wide variety of digital projects. Over two evenings Melissa Eggleston, ux/content strategist, and Julie Grundy, information architect, will cover tools for planning a successful user experience, including inherent design principles, introduction to information architecture and user research, and common ux pitfalls to avoid. 

Anyone is encouraged to attend - designers, developers, marketers or just those with an interest in making the world a more user-friendly place. No coding experience necessary.